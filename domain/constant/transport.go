package constant

type Transport int

const (
	BUS Transport = iota + 1
	TRAIN
	FLIGHT
)

var (
	// transportDict is a
	transportDict = []string{"BUS", "TRAIN", "FLIGHT"}
)

// String is a method
func (t Transport) String() string {
	return transportDict[t]
}

// TransportList is a function
func TransportList() []string {
	return transportDict
}
