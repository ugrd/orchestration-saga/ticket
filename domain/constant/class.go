package constant

type TransportClass int

const (
	EXECUTIVE TransportClass = iota + 1
	ECONOMY
)

var (
	// transportClassDict is a list
	transportClassDict = []string{"EXECUTIVE", "ECONOMY"}
)

// String is a method
func (t TransportClass) String() string {
	return transportDict[t]
}

// TransportClassList is a function
func TransportClassList() []string {
	return transportClassDict
}
