package constant

type Passenger int

type PassengerObject struct {
	Name   string `json:"name"`
	AgeMin uint8  `json:"age_min"`
	AgeMax uint8  `json:"age_max"`
}

const (
	Adult Passenger = iota + 1
	Infant
)

var (
	// passengerDict is a collection of passenger types
	passengerDict = []string{"ADULT", "INFANT"}
)

// String is a method
func (p Passenger) String() string {
	return passengerDict[p]
}

// PassengerList is a function
func PassengerList() []string {
	return passengerDict
}

// PassengerObjectList is a function
func PassengerObjectList() []PassengerObject {
	return []PassengerObject{
		{
			Name:   "INFANT",
			AgeMin: 1,
			AgeMax: 3,
		},
		{
			Name:   "ADULT",
			AgeMin: 4,
			AgeMax: 100,
		},
	}
}
