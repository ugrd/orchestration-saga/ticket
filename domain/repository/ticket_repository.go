package repository

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/catalog/pkg/common"
)

// TicketRepoInterface is ticket repo contract
type TicketRepoInterface interface {
	Create(ctx context.Context, ticket *entity.Ticket) error
	Update(ctx context.Context, ticket *entity.Ticket, id uint) error
	Delete(ctx context.Context, id uint) error
	FindByID(ctx context.Context, id uint) (*entity.Ticket, error)
	FindByNo(ctx context.Context, no string) (*entity.Ticket, error)
	FindAll(ctx context.Context, filter *common.FilterQuery) ([]*entity.Ticket, error)
	FindSearch(ctx context.Context, query *entity.SearchFilterQuery) ([]*entity.Ticket, error)
}
