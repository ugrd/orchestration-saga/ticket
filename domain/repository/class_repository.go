package repository

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/catalog/pkg/common"
)

// ClassRepoInterface is a class repo contract
type ClassRepoInterface interface {
	Create(ctx context.Context, service *entity.Class) error
	Update(ctx context.Context, service *entity.Class, id uint) error
	Delete(ctx context.Context, id uint) error
	FindByID(ctx context.Context, id uint) (*entity.Class, error)
	FindByCode(ctx context.Context, code string) (*entity.Class, error)
	FindAll(ctx context.Context, filter *common.FilterQuery) ([]*entity.Class, error)
}
