package repository

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/catalog/pkg/common"
)

// ProviderRepoInterface is a class repo contract
type ProviderRepoInterface interface {
	Create(ctx context.Context, provider *entity.Provider) error
	Update(ctx context.Context, provider *entity.Provider, id uint) error
	Delete(ctx context.Context, id uint) error
	FindByID(ctx context.Context, id uint) (*entity.Provider, error)
	FindByCode(ctx context.Context, code string) (*entity.Provider, error)
	FindAll(ctx context.Context, filter *common.FilterQuery) ([]*entity.Provider, error)
}
