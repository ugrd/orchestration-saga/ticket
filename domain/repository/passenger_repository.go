package repository

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/catalog/pkg/common"
)

// PassengerRepoInterface is a repo interface
type PassengerRepoInterface interface {
	Create(ctx context.Context, passenger *entity.Passenger) error
	Update(ctx context.Context, passenger *entity.Passenger, id uint) error
	Delete(ctx context.Context, id uint) error
	FindByID(ctx context.Context, id uint) (*entity.Passenger, error)
	FindByName(ctx context.Context, name string) (*entity.Passenger, error)
	FindAll(ctx context.Context, filter *common.FilterQuery) ([]*entity.Passenger, error)
}
