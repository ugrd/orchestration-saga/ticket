package repository

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/catalog/pkg/common"
)

// TransportShelterRepoInterface is a repo interface
type TransportShelterRepoInterface interface {
	Create(ctx context.Context, transportShelter *entity.TransportShelter) error
	Update(ctx context.Context, transportShelter *entity.TransportShelter, id uint) error
	Delete(ctx context.Context, id uint) error
	FindByID(ctx context.Context, id uint) (*entity.TransportShelter, error)
	FindByName(ctx context.Context, name string) (*entity.TransportShelter, error)
	FindByCode(ctx context.Context, code string) (*entity.TransportShelter, error)
	FindByTransportID(ctx context.Context, transportID uint) ([]*entity.TransportShelter, error)
	FindAll(ctx context.Context, filter *common.FilterQuery) ([]*entity.TransportShelter, error)
}
