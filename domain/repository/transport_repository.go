package repository

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/catalog/pkg/common"
)

// TransportRepoInterface is category repo contract
type TransportRepoInterface interface {
	Create(ctx context.Context, category *entity.Transport) error
	Update(ctx context.Context, category *entity.Transport, id uint) error
	Delete(ctx context.Context, id uint) error
	FindByID(ctx context.Context, id uint) (*entity.Transport, error)
	FindBySlug(ctx context.Context, slug string) (*entity.Transport, error)
	FindAll(ctx context.Context, filter *common.FilterQuery) ([]*entity.Transport, error)
}
