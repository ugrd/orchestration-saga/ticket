package entity

import "strings"

// Interface is a set of functions that should be implemented by entity
type Interface interface {
	TableName() string
	FilterableFields() []interface{}
	TimeFields() []interface{}
}

// ConvertToCode is a function
func ConvertToCode(str string) string {
	return strings.Join(strings.Split(strings.ToUpper(str), " "), "-")
}

const (
	// DefaultOffset is a default pagination offset
	DefaultOffset = 0
	// DefaultLimit is a default pagination limit
	DefaultLimit = 10
)
