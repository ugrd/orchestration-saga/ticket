package entity

import (
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/contract"
	"gorm.io/gorm"
	"time"
)

// TransportShelter is a struct that represents transport_shelters table
type TransportShelter struct {
	ID          uint           `gorm:"column:id;not null;uniqueIndex;primaryKey" json:"id"`
	TransportID uint           `gorm:"column:transport_id; not null;" json:"transport_id"`
	Name        string         `gorm:"column:name;size:100;not null;" json:"name"`
	Code        string         `gorm:"column:code;size:100;not null;index; default:''" json:"code"`
	Province    string         `gorm:"column:province;size:100;not null;index; default:''" json:"province"`
	City        string         `gorm:"column:city;size:100;not null;index; default:''" json:"city"`
	Address     string         `gorm:"column:address;size:255;not null;default:''" json:"address"`
	CreatedAt   time.Time      `json:"created_at"`
	UpdatedAt   time.Time      `json:"updated_at"`
	DeletedAt   gorm.DeletedAt `json:"deleted_at"`
	Transport   Transport      `json:"transport"`
}

// Implements base entity methods
var _ contract.EntityInterface = &TransportShelter{}

// TableName is a function return table name
func (u *TransportShelter) TableName() string {
	return "transport_shelters"
}

// FilterableFields is a function return filterable fields
func (u *TransportShelter) FilterableFields() []interface{} {
	return []interface{}{"name", "code", "description"}
}

// TimeFields is a function return time fields
func (u *TransportShelter) TimeFields() []interface{} {
	return []interface{}{"created_at", "updated_at", "deleted_at"}
}
