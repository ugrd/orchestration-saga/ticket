package entity

import (
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/contract"
	"time"
)

// Class is a struct that represents classes table
type Class struct {
	ID          uint      `gorm:"column:id;not null;uniqueIndex;primaryKey" json:"id"`
	Name        string    `gorm:"column:name;size:100;not null;" json:"name"` // EXECUTIVE, ECONOMY
	Code        string    `gorm:"column:code;size:100;not null;index; default:''" json:"code"`
	Description string    `gorm:"column:description;type:text;not null;default:''" json:"description"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

// Implements base entity methods
var _ contract.EntityInterface = &Class{}

// TableName is a function return table name
func (u *Class) TableName() string {
	return "classes"
}

// FilterableFields is a function return filterable fields
func (u *Class) FilterableFields() []interface{} {
	return []interface{}{"name", "code", "description"}
}

// TimeFields is a function return time fields
func (u *Class) TimeFields() []interface{} {
	return []interface{}{"created_at", "updated_at", "deleted_at"}
}
