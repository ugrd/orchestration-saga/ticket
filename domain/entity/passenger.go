package entity

import (
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/contract"
	"time"
)

// Passenger is a struct that represents passengers table
type Passenger struct {
	ID          uint      `gorm:"column:id;not null;uniqueIndex;primaryKey" json:"id"`
	Name        string    `gorm:"column:name;size:100;not null;" json:"name"` // ADULT
	AgeMin      uint8     `gorm:"column:age_min;not null" json:"age_min"`
	AgeMax      uint8     `gorm:"column:age_max;not null" json:"age_max"`
	Description string    `gorm:"column:description;type:text;not null;default:''" json:"description"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

// Implements base entity methods
var _ contract.EntityInterface = &Passenger{}

// TableName is a function return table name
func (u *Passenger) TableName() string {
	return "passengers"
}

// FilterableFields is a function return filterable fields
func (u *Passenger) FilterableFields() []interface{} {
	return []interface{}{"name", "age_min", "age_max"}
}

// TimeFields is a function return time fields
func (u *Passenger) TimeFields() []interface{} {
	return []interface{}{"created_at", "updated_at", "deleted_at"}
}
