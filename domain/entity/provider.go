package entity

import (
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/contract"
	"gorm.io/gorm"
	"time"
)

// Provider is a struct that represents providers table
type Provider struct {
	ID          uint           `gorm:"column:id;not null;uniqueIndex;primaryKey" json:"id"`
	TransportID uint           `gorm:"column:transport_id; not null;" json:"transport_id"`
	Name        string         `gorm:"column:name;size:100;not null;" json:"name"` // EXECUTIVE, ECONOMY
	Code        string         `gorm:"column:code;size:100;not null;index; default:''" json:"code"`
	Description string         `gorm:"column:description;type:text;not null;default:''" json:"description"`
	CreatedAt   time.Time      `json:"created_at"`
	UpdatedAt   time.Time      `json:"updated_at"`
	DeletedAt   gorm.DeletedAt `json:"deleted_at"`
	Transport   Transport      `json:"transport"`
}

// Implements base entity methods
var _ contract.EntityInterface = &Provider{}

// TableName is a function return table name
func (u *Provider) TableName() string {
	return "providers"
}

// FilterableFields is a function return filterable fields
func (u *Provider) FilterableFields() []interface{} {
	return []interface{}{"name", "code", "description"}
}

// TimeFields is a function return time fields
func (u *Provider) TimeFields() []interface{} {
	return []interface{}{"created_at", "updated_at", "deleted_at"}
}
