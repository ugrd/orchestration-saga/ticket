package entity

import (
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/contract"
	"time"
)

// Transport is a struct that represents transports table
type Transport struct {
	ID          uint      `gorm:"column:id;not null;uniqueIndex;primaryKey" json:"id"`
	Name        string    `gorm:"column:name;size:100;not null;" json:"name"` // BUS, TRAIN, FLIGHT
	Slug        string    `gorm:"column:slug;size:100;not null;index; default:''" json:"slug"`
	Description string    `gorm:"column:description;type:text;not null;default:''" json:"description"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

// Implements base entity methods
var _ contract.EntityInterface = &Transport{}

// TableName is a function return table name
func (u *Transport) TableName() string {
	return "transports"
}

// FilterableFields is a function return filterable fields
func (u *Transport) FilterableFields() []interface{} {
	return []interface{}{"name", "slug", "description"}
}

// TimeFields is a function return time fields
func (u *Transport) TimeFields() []interface{} {
	return []interface{}{"created_at", "updated_at", "deleted_at"}
}
