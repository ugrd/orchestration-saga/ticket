package entity

import (
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/contract"
	"gorm.io/gorm"
	"time"
)

// Ticket is a struct that represents tickets table
type Ticket struct {
	ID            uint             `gorm:"column:id;not null;uniqueIndex;primaryKey" json:"id"`
	No            string           `gorm:"column:no;not null;uniqueIndex" json:"no"`
	TransportID   uint             `gorm:"column:transport_id; not null;" json:"transport_id"`
	ClassID       uint             `gorm:"column:class_id; not null;" json:"class_id"`
	OriginID      uint             `gorm:"column:origin_id; not null;" json:"origin_id"`
	DestinationID uint             `gorm:"column:destination_id; not null;" json:"destination_id"`
	ProviderID    uint             `gorm:"column:provider_id; not null;" json:"provider_id"`
	DepartureTime time.Time        `gorm:"column:departure_time;not null'" json:"departure_time"`
	ArriveTime    time.Time        `gorm:"column:arrive_time;not null'" json:"arrive_time"`
	Price         uint64           `gorm:"column:price;not null" json:"price"`
	Quantity      int32            `gorm:"column:quantity;not null" json:"quantity"`
	CreatedAt     time.Time        `json:"created_at"`
	UpdatedAt     time.Time        `json:"updated_at"`
	DeletedAt     gorm.DeletedAt   `json:"deleted_at"`
	Transport     Transport        `json:"transport"`
	Class         Class            `json:"class"`
	Origin        TransportShelter `gorm:"foreignKey:OriginID" json:"origin"`
	Destination   TransportShelter `gorm:"foreignKey:DestinationID" json:"destination"`
	Provider      Provider         `json:"provider"`
}

// SearchFilterQuery is a struct
type SearchFilterQuery struct {
	TransportID   uint      `json:"transport_id"`
	ClassID       uint      `json:"class_id"`
	DepartureTime time.Time `json:"departure_time"`
}

// Implements base entity methods
var _ contract.EntityInterface = &Ticket{}

// TableName is a function return table name
func (u *Ticket) TableName() string {
	return "tickets"
}

// FilterableFields is a function return filterable fields
func (u *Ticket) FilterableFields() []interface{} {
	return []interface{}{}
}

// TimeFields is a function return time fields
func (u *Ticket) TimeFields() []interface{} {
	return []interface{}{}
}
