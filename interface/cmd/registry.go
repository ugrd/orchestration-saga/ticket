package cmd

import "github.com/urfave/cli/v2"

// Build is a method
func (cmd *Command) Build() []*cli.Command {
	cmd.registerCLI(cmd.newDBMigrate())
	cmd.registerCLI(cmd.newDBSeed())
	cmd.registerCLI(cmd.newMakeSecret())

	return cmd.CLI
}
