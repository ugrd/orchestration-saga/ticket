package handler

import (
	"context"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"
)

// UpdateClass is a method
func (hdl *Handler) UpdateClass(ctx context.Context, req *catalog.UpdateClassRequest) (*catalog.Class, error) {
	payload := &UpdateClassPayload{
		ID:          req.GetId(),
		Name:        req.GetName(),
		Description: req.GetDescription(),
	}

	if err := payload.Validate(); err != nil {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(utils.ErrToMap(err)).
			ErrList()
	}

	r, errFindByID := hdl.Dependency.Repository.Class.FindByID(ctx, uint(req.GetId()))
	if errFindByID != nil {
		switch errFindByID {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Data not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(errFindByID.Error()).
				Err()
		}
	}

	code := entity.ConvertToCode(req.GetName())

	_, errFindBySlug := hdl.Dependency.Repository.Class.FindByCode(ctx, code)
	if errFindBySlug == nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.AlreadyExists).
			WithFieldsFromMap(map[string]interface{}{
				"name": fmt.Sprintf("already exist"),
			}).
			ErrList()
	}

	if r.Code == code {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.AlreadyExists).
			WithFieldsFromMap(map[string]interface{}{
				"name": fmt.Sprintf("already exist"),
			}).
			ErrList()
	}

	err := hdl.Dependency.Repository.Class.Update(ctx, &entity.Class{
		Name:        payload.Name,
		Code:        code,
		Description: payload.Description,
	}, uint(req.GetId()))
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	r, _ = hdl.Dependency.Repository.Class.FindByID(ctx, uint(req.GetId()))
	return &catalog.Class{
		Id:          uint64(r.ID),
		Name:        r.Name,
		Code:        r.Code,
		Description: r.Description,
		CreatedAt:   timestamppb.New(r.CreatedAt),
		UpdatedAt:   timestamppb.New(r.UpdatedAt),
	}, nil
}

// UpdateClassPayload is a type
type UpdateClassPayload struct {
	ID          uint64 `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

// Validate is a method
func (c UpdateClassPayload) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.ID, validation.Required),
		validation.Field(&c.Name, validation.Required, validation.Length(3, 100)),
	)
}
