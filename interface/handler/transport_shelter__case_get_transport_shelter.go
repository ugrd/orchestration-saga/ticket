package handler

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/catalog/pkg/common"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// GetListTransportShelter is a method
func (hdl *Handler) GetListTransportShelter(ctx context.Context, query *catalog.TransportShelterFilterQuery) (*catalog.ListTransportShelter, error) {
	var transportShelters []*catalog.TransportShelter

	rows, err := hdl.Dependency.Repository.TransportShelterRepo.FindAll(ctx, &common.FilterQuery{})
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	for _, row := range rows {
		transportShelters = append(transportShelters, &catalog.TransportShelter{
			Id:          uint64(row.ID),
			TransportId: uint64(row.TransportID),
			Name:        row.Name,
			Code:        row.Code,
			Province:    row.Province,
			City:        row.City,
			Address:     row.Address,
			CreatedAt:   timestamppb.New(row.CreatedAt),
			UpdatedAt:   timestamppb.New(row.UpdatedAt),
		})
	}

	return &catalog.ListTransportShelter{
		TransportShelters: transportShelters,
	}, nil
}
