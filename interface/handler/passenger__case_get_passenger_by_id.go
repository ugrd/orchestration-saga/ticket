package handler

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"
	"reflect"
)

// GetPassengerByID is a method
func (hdl *Handler) GetPassengerByID(ctx context.Context, req *catalog.PassengerIdentifierRequest) (*catalog.Passenger, error) {
	id := req.GetId()
	if reflect.ValueOf(id).IsZero() {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(map[string]interface{}{
				"id": "is not valid",
			}).ErrList()
	}

	r, err := hdl.Dependency.Repository.PassengerRepo.FindByID(ctx, uint(id))
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Data not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	return &catalog.Passenger{
		Id:        uint64(r.ID),
		Name:      r.Name,
		AgeMax:    uint32(r.AgeMax),
		AgeMin:    uint32(r.AgeMin),
		CreatedAt: timestamppb.New(r.CreatedAt),
		UpdatedAt: timestamppb.New(r.UpdatedAt),
	}, nil
}
