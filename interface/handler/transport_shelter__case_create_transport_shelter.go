package handler

import (
	"context"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"
)

// CreateTransportShelter is a method
func (hdl *Handler) CreateTransportShelter(ctx context.Context, req *catalog.CreateTransportShelterRequest) (*catalog.TransportShelter, error) {
	payload := &CreateTransportShelterPayload{
		TransportID: req.GetTransportId(),
		Name:        req.GetName(),
		Code:        req.GetCode(),
		Province:    req.GetProvince(),
		City:        req.GetCity(),
		Address:     req.GetAddress(),
	}

	if err := payload.Validate(); err != nil {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(utils.ErrToMap(err)).
			ErrList()
	}

	_, errNotFound := hdl.Dependency.Repository.Transport.FindByID(ctx, uint(req.GetTransportId()))
	if errNotFound != nil {
		switch errNotFound {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("transport_id not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(errNotFound.Error()).
				Err()
		}
	}

	_, ok := hdl.Dependency.Repository.TransportShelterRepo.FindByName(ctx, req.GetCode())
	if ok == nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.AlreadyExists).
			WithFieldsFromMap(map[string]interface{}{
				"name": fmt.Sprintf("already exist"),
			}).
			ErrList()
	}

	_, ok = hdl.Dependency.Repository.TransportShelterRepo.FindByCode(ctx, req.GetCode())
	if ok == nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.AlreadyExists).
			WithFieldsFromMap(map[string]interface{}{
				"name": fmt.Sprintf("already exist"),
			}).
			ErrList()
	}

	r := &entity.TransportShelter{
		TransportID: uint(req.GetTransportId()),
		Name:        req.GetName(),
		Code:        req.GetCode(),
		Province:    req.GetProvince(),
		City:        req.GetCity(),
		Address:     req.GetAddress(),
	}

	err := hdl.Dependency.Repository.TransportShelterRepo.Create(ctx, r)
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	return &catalog.TransportShelter{
		Id:        uint64(r.ID),
		Name:      r.Name,
		Code:      r.Code,
		Province:  r.Province,
		City:      r.City,
		Address:   r.Address,
		CreatedAt: timestamppb.New(r.CreatedAt),
		UpdatedAt: timestamppb.New(r.UpdatedAt),
	}, nil
}

// CreateTransportShelterPayload is a type
type CreateTransportShelterPayload struct {
	TransportID uint64 `json:"transport_id"`
	Name        string `json:"name"`
	Code        string `json:"code"`
	Province    string `json:"province"`
	City        string `json:"city"`
	Address     string `json:"address"`
}

// Validate is a method
func (c CreateTransportShelterPayload) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.TransportID, validation.Required),
		validation.Field(&c.Name, validation.Required, validation.Length(3, 100)),
		validation.Field(&c.Code, validation.Required, validation.Length(3, 100)),
		validation.Field(&c.Province, validation.Required, validation.Length(3, 100)),
		validation.Field(&c.City, validation.Required, validation.Length(3, 100)),
		validation.Field(&c.Address, validation.Required, validation.Length(10, 255)),
	)
}
