package handler

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
)

// DeleteClass is a method
func (hdl *Handler) DeleteClass(ctx context.Context, req *catalog.ClassIdentifierRequest) (*catalog.DeleteClassResponse, error) {
	return nil, exception.NewGRPCError().
		WithErrCode(codes.PermissionDenied).
		WithMsg(fmt.Errorf("forbidden to process").Error()).
		Err()
}
