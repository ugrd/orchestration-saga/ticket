package handler

import (
	"context"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/catalog/infrastructure/dto"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	log "gitlab.com/ugrd/orchestration-saga/package/logger"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
	"gorm.io/gorm"
)

// IncreaseTicketQty is a method
func (hdl *Handler) IncreaseTicketQty(ctx context.Context, req *catalog.IncreaseTicketQtyRequest) (*catalog.Ticket, error) {
	logger := log.New()

	logger.Info("[IncreaseTicketQty] starting....")

	payload := &DecreaseTicketQtyPayload{
		ID:  req.GetId(),
		Qty: req.GetQty(),
	}

	logger.Infof("[IncreaseTicketQty] args: ID: %d, Qty: %d", req.GetId(), req.GetQty())

	if err := payload.Validate(); err != nil {
		logger.Warnf("[IncreaseTicketQty] error validation %v:", err)
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(utils.ErrToMap(err)).
			ErrList()
	}

	r, err := hdl.Dependency.Repository.TicketRepo.FindByID(ctx, uint(req.GetId()))
	if err != nil {
		logger.Warnf("[IncreaseTicketQty] error find ticket by id  %v:", err)
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Data not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	qty := uint32(r.Quantity) + req.GetQty()

	err = hdl.Dependency.Repository.TicketRepo.Update(ctx, &entity.Ticket{
		Quantity: int32(qty),
	}, uint(req.GetId()))
	if err != nil {
		logger.Warnf("[IncreaseTicketQty] error increase quantity :%v", err)
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	r, _ = hdl.Dependency.Repository.TicketRepo.FindByID(ctx, uint(req.GetId()))

	logger.Infof("[DecreaseTicketQty] current quantity %d", r.Quantity)

	return dto.TransformTicketToRPCResponse(r), nil
}

// IncreaseTicketQtyPayload is a method
type IncreaseTicketQtyPayload struct {
	ID  uint64
	Qty uint32
}

// Validate is a method
func (c IncreaseTicketQtyPayload) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.ID, validation.Required),
		validation.Field(&c.Qty, validation.Required),
	)
}
