package handler

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/catalog/infrastructure/dto"
	"gitlab.com/ugrd/orchestration-saga/catalog/interface/validator"
	"gitlab.com/ugrd/orchestration-saga/catalog/util"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
	"gorm.io/gorm"
)

// CreateTicket is a method
func (hdl *Handler) CreateTicket(ctx context.Context, req *catalog.CreateTicketRequest) (*catalog.Ticket, error) {
	payload := &validator.CreateTicketPayload{
		ProviderID:    req.GetProviderId(),
		TransportID:   req.GetTransportId(),
		ClassID:       req.GetClassId(),
		OriginID:      req.GetOriginId(),
		DestinationID: req.GetDestinationId(),
		DepartureTime: req.GetDepartureTime(),
		ArriveTime:    req.GetArriveTime(),
		Price:         req.GetPrice(),
		Quantity:      req.GetQuantity(),
	}

	if err := payload.Validate(); err != nil {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(utils.ErrToMap(err)).
			ErrList()
	}

	transport, err := hdl.Dependency.Repository.Transport.FindByID(ctx, uint(req.GetTransportId()))
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Transport not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	_, err = hdl.Dependency.Repository.Class.FindByID(ctx, uint(req.GetClassId()))
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Class not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	_, err = hdl.Dependency.Repository.ProviderRepo.FindByID(ctx, uint(req.GetProviderId()))
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Provider not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	_, err = hdl.Dependency.Repository.TransportShelterRepo.FindByID(ctx, uint(req.GetOriginId()))
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Origin not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	_, err = hdl.Dependency.Repository.TransportShelterRepo.FindByID(ctx, uint(req.GetDestinationId()))
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Destination not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	departureTime, _ := util.StringToTime(req.GetDepartureTime())
	arriveTime, _ := util.StringToTime(req.GetArriveTime())

	r := &entity.Ticket{
		No:            util.GenerateTicketToken(transport.Name),
		TransportID:   uint(req.GetTransportId()),
		ClassID:       uint(req.GetClassId()),
		ProviderID:    uint(req.GetProviderId()),
		OriginID:      uint(req.GetOriginId()),
		DestinationID: uint(req.GetDestinationId()),
		DepartureTime: *departureTime,
		ArriveTime:    *arriveTime,
		Price:         req.GetPrice(),
		Quantity:      int32(req.GetQuantity()),
	}

	err = hdl.Dependency.Repository.TicketRepo.Create(ctx, r)
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	r, _ = hdl.Dependency.Repository.TicketRepo.FindByID(ctx, r.ID)

	return dto.TransformTicketToRPCResponse(r), nil
}
