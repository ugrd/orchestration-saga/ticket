package handler

import (
	"context"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
	"strings"
)

// CreatePassenger is a method
func (hdl *Handler) CreatePassenger(ctx context.Context, req *catalog.CreatePassengerRequest) (*catalog.Passenger, error) {
	payload := &CreatePassengerPayload{
		Name:   req.GetName(),
		AgeMin: uint8(req.GetAgeMin()),
		AgeMax: uint8(req.GetAgeMax()),
	}

	if err := payload.Validate(); err != nil {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(utils.ErrToMap(err)).
			ErrList()
	}

	_, ok := hdl.Dependency.Repository.PassengerRepo.FindByName(ctx, strings.ToLower(req.GetName()))
	if ok == nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.AlreadyExists).
			WithFieldsFromMap(map[string]interface{}{
				"name": fmt.Sprintf("already exist"),
			}).
			ErrList()
	}

	r := &entity.Passenger{
		Name:   req.GetName(),
		AgeMax: uint8(req.GetAgeMax()),
		AgeMin: uint8(req.GetAgeMin()),
	}

	err := hdl.Dependency.Repository.PassengerRepo.Create(ctx, r)
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	return &catalog.Passenger{
		Id:        uint64(r.ID),
		Name:      r.Name,
		AgeMin:    uint32(r.AgeMin),
		AgeMax:    uint32(r.AgeMax),
		CreatedAt: timestamppb.New(r.CreatedAt),
		UpdatedAt: timestamppb.New(r.UpdatedAt),
	}, nil
}

// CreatePassengerPayload is a type
type CreatePassengerPayload struct {
	Name   string `json:"name"`
	AgeMax uint8  `json:"age_max"`
	AgeMin uint8  `json:"age_min"`
}

// Validate is a method
func (c CreatePassengerPayload) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.Name, validation.Required, validation.Length(3, 100)),
		validation.Field(&c.AgeMax, validation.Required),
		validation.Field(&c.AgeMin, validation.Required),
	)
}
