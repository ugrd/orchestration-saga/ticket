package handler

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
)

// DeleteTransport is a method
func (hdl *Handler) DeleteTransport(ctx context.Context, req *catalog.TransportIdentifierRequest) (*catalog.DeleteTransportResponse, error) {
	return nil, exception.NewGRPCError().
		WithErrCode(codes.PermissionDenied).
		WithMsg(fmt.Errorf("forbidden to process").Error()).
		Err()
}
