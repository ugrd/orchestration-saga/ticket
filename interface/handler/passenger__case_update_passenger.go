package handler

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
)

// UpdatePassenger is a method
func (hdl *Handler) UpdatePassenger(_ context.Context, _ *catalog.UpdatePassengerRequest) (*catalog.Passenger, error) {
	return nil, exception.NewGRPCError().
		WithErrCode(codes.PermissionDenied).
		WithMsg(fmt.Errorf("forbidden to process").Error()).
		Err()
}
