package handler

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/catalog/pkg/common"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// GetListProvider is a method
func (hdl *Handler) GetListProvider(ctx context.Context, query *catalog.ProviderFilterQuery) (*catalog.ListProvider, error) {
	var providers []*catalog.Provider

	rows, err := hdl.Dependency.Repository.ProviderRepo.FindAll(ctx, &common.FilterQuery{})
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	for _, row := range rows {
		providers = append(providers, &catalog.Provider{
			Id:          uint64(row.ID),
			Name:        row.Name,
			Code:        row.Code,
			Description: row.Description,
			CreatedAt:   timestamppb.New(row.CreatedAt),
			UpdatedAt:   timestamppb.New(row.UpdatedAt),
		})
	}

	return &catalog.ListProvider{
		Providers: providers,
	}, nil
}
