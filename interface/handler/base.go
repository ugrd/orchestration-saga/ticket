package handler

import (
	"gitlab.com/ugrd/orchestration-saga/catalog/interface/handler/core"
	"gitlab.com/ugrd/orchestration-saga/catalog/interface/handler/dependency"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
)

// Interface is an interface
type Interface interface {
	catalog.ClassServiceServer
	catalog.TransportServiceServer
	catalog.ProviderServiceServer
	catalog.PassengerServiceServer
	catalog.TransportShelterServiceServer
	catalog.TicketServiceServer
}

// Handler is struct
type Handler struct {
	*dependency.Dependency
	*core.GRPCService
}

// New is a constructor
func New(opts ...Option) *Handler {
	handler := &Handler{
		Dependency: &dependency.Dependency{},
	}

	for _, opt := range opts {
		opt(handler)
	}

	return handler
}

var _ Interface = &Handler{}
