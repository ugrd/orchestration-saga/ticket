package handler

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"
	"reflect"
)

// GetTransportShelterByID is a method
func (hdl *Handler) GetTransportShelterByID(ctx context.Context, req *catalog.TransportShelterIdentifierRequest) (*catalog.TransportShelter, error) {
	id := req.GetId()
	if reflect.ValueOf(id).IsZero() {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(map[string]interface{}{
				"id": "is not valid",
			}).ErrList()
	}

	r, err := hdl.Dependency.Repository.TransportShelterRepo.FindByID(ctx, uint(id))
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Data not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	return &catalog.TransportShelter{
		Id:        uint64(r.ID),
		Name:      r.Name,
		Code:      r.Code,
		Province:  r.Province,
		City:      r.City,
		Address:   r.Address,
		CreatedAt: timestamppb.New(r.CreatedAt),
		UpdatedAt: timestamppb.New(r.UpdatedAt),
	}, nil
}
