package handler

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/catalog/infrastructure/dto"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
	"gorm.io/gorm"
	"reflect"
)

// GetTicketByNo is a method
func (hdl *Handler) GetTicketByNo(ctx context.Context, req *catalog.TicketIdentifierByNoRequest) (*catalog.Ticket, error) {
	no := req.GetNo()
	if reflect.ValueOf(no).IsZero() {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(map[string]interface{}{
				"no": "is not valid",
			}).ErrList()
	}

	r, err := hdl.Dependency.Repository.TicketRepo.FindByNo(ctx, no)
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Data not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	return dto.TransformTicketToRPCResponse(r), nil
}
