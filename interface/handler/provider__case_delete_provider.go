package handler

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
)

// DeleteProvider is a method
func (hdl *Handler) DeleteProvider(_ context.Context, _ *catalog.ProviderIdentifierRequest) (*catalog.DeleteProviderResponse, error) {
	return nil, exception.NewGRPCError().
		WithErrCode(codes.PermissionDenied).
		WithMsg(fmt.Errorf("forbidden to process").Error()).
		Err()
}
