package handler

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/catalog/infrastructure/dto"
	"gitlab.com/ugrd/orchestration-saga/catalog/pkg/common"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
)

// GetListTicket is a method
func (hdl *Handler) GetListTicket(ctx context.Context, _ *catalog.TicketFilterQuery) (*catalog.ListTicket, error) {
	rows, err := hdl.Dependency.Repository.TicketRepo.FindAll(ctx, &common.FilterQuery{})
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	return dto.TransformTicketListToRPCResponse(rows), nil
}
