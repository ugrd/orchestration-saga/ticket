package handler

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
	"gorm.io/gorm"
	"reflect"
)

// DeleteTicket is a method
func (hdl *Handler) DeleteTicket(ctx context.Context, req *catalog.TicketIdentifierRequest) (*catalog.DeleteTicketRequest, error) {
	id := req.GetId()
	if reflect.ValueOf(id).IsZero() {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(map[string]interface{}{
				"id": "is not valid",
			}).ErrList()
	}

	_, err := hdl.Dependency.Repository.TicketRepo.FindByID(ctx, uint(id))
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Data not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	err = hdl.Dependency.Repository.TicketRepo.Delete(ctx, uint(id))
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	return &catalog.DeleteTicketRequest{
		Message: "Ok",
	}, nil
}
