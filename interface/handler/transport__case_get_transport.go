package handler

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/catalog/pkg/common"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// GetListTransport is a method to retrieve transport
func (hdl *Handler) GetListTransport(ctx context.Context, _ *catalog.TransportFilterQuery) (*catalog.ListTransport, error) {
	var transports []*catalog.Transport

	rows, err := hdl.Dependency.Repository.Transport.FindAll(ctx, &common.FilterQuery{})
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	for _, row := range rows {
		transports = append(transports, &catalog.Transport{
			Id:          uint64(row.ID),
			Name:        row.Name,
			Slug:        row.Slug,
			Description: row.Description,
			CreatedAt:   timestamppb.New(row.CreatedAt),
			UpdatedAt:   timestamppb.New(row.UpdatedAt),
		})
	}

	return &catalog.ListTransport{
		Transports: transports,
	}, nil
}
