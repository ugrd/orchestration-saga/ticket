package handler

import (
	"context"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// CreateTransport is a method to create new transport
func (hdl *Handler) CreateTransport(ctx context.Context, req *catalog.CreateTransportRequest) (*catalog.Transport, error) {
	payload := &TransportCreatePayload{
		Transport: &entity.Transport{
			Name:        req.GetName(),
			Slug:        utils.StringToSlug(req.GetName()),
			Description: req.GetDescription(),
		},
	}

	errValidation := payload.Validate()
	if errValidation != nil {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(utils.ErrToMap(errValidation)).
			ErrList()
	}

	_, ok := hdl.Dependency.Repository.Transport.FindBySlug(ctx, utils.StringToSlug(req.GetName()))
	if ok == nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.AlreadyExists).
			WithFieldsFromMap(map[string]interface{}{
				"name": fmt.Sprintf("already exist"),
			}).
			ErrList()
	}

	err := hdl.Dependency.Repository.Transport.Create(ctx, payload.Transport)
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	cID := payload.Transport.ID
	r, _ := hdl.Dependency.Repository.Transport.FindByID(ctx, cID)

	return &catalog.Transport{
		Id:          uint64(r.ID),
		Name:        r.Name,
		Slug:        r.Slug,
		Description: r.Description,
		CreatedAt:   timestamppb.New(r.CreatedAt),
		UpdatedAt:   timestamppb.New(r.UpdatedAt),
	}, nil
}

// TransportCreatePayload is a struct
type TransportCreatePayload struct {
	*entity.Transport
}

// Validate is a method to validate
func (c TransportCreatePayload) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.Transport.Name, validation.Required, validation.Length(3, 100)),
	)
}
