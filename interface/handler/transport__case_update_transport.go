package handler

import (
	"context"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"
)

// UpdateTransport is a method
func (hdl *Handler) UpdateTransport(ctx context.Context, req *catalog.UpdateTransportRequest) (*catalog.Transport, error) {
	payload := &TransportUpdatePayload{
		ID:          int64(req.GetId()),
		Name:        req.GetName(),
		Slug:        utils.StringToSlug(req.GetName()),
		Description: req.GetDescription(),
	}

	if err := payload.Validate(); err != nil {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(utils.ErrToMap(err)).
			ErrList()
	}

	r, errFindByID := hdl.Dependency.Repository.Transport.FindByID(ctx, uint(req.GetId()))
	if errFindByID != nil {
		switch errFindByID {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Data not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(errFindByID.Error()).
				Err()
		}
	}

	_, errFindBySlug := hdl.Dependency.Repository.Transport.FindBySlug(ctx, payload.Slug)
	if errFindBySlug == nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.AlreadyExists).
			WithFieldsFromMap(map[string]interface{}{
				"name": fmt.Sprintf("already exist"),
			}).
			ErrList()
	}

	if r.Slug == payload.Slug {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.AlreadyExists).
			WithFieldsFromMap(map[string]interface{}{
				"name": fmt.Sprintf("already exist"),
			}).
			ErrList()
	}

	err := hdl.Dependency.Repository.Transport.Update(ctx, &entity.Transport{
		Name:        payload.Name,
		Slug:        payload.Slug,
		Description: payload.Description,
	}, uint(req.GetId()))
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	r, _ = hdl.Dependency.Repository.Transport.FindByID(ctx, uint(req.GetId()))
	return &catalog.Transport{
		Id:          uint64(r.ID),
		Name:        r.Name,
		Slug:        r.Slug,
		Description: r.Description,
		CreatedAt:   timestamppb.New(r.CreatedAt),
		UpdatedAt:   timestamppb.New(r.UpdatedAt),
	}, nil
}

// TransportUpdatePayload is a type
type TransportUpdatePayload struct {
	ID          int64  `json:"id"`
	Name        string `json:"name"`
	Slug        string `json:"slug"`
	Description string `json:"description"`
}

// Validate is a method
func (c TransportUpdatePayload) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.ID, validation.Required),
		validation.Field(&c.Name, validation.Required, validation.Length(3, 100)),
	)
}
