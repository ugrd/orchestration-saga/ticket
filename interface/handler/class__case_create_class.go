package handler

import (
	"context"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// CreateClass is a method
func (hdl *Handler) CreateClass(ctx context.Context, req *catalog.CreateClassRequest) (*catalog.Class, error) {
	payload := &CreateClassPayload{
		Name:        req.GetName(),
		Description: req.GetDescription(),
	}

	if err := payload.Validate(); err != nil {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(utils.ErrToMap(err)).
			ErrList()
	}

	code := entity.ConvertToCode(req.GetName())
	_, ok := hdl.Dependency.Repository.Class.FindByCode(ctx, code)
	if ok == nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.AlreadyExists).
			WithFieldsFromMap(map[string]interface{}{
				"name": fmt.Sprintf("already exist"),
			}).
			ErrList()
	}

	r := &entity.Class{
		Name:        req.GetName(),
		Code:        code,
		Description: req.GetDescription(),
	}

	err := hdl.Dependency.Repository.Class.Create(ctx, r)
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	return &catalog.Class{
		Id:          uint64(r.ID),
		Name:        r.Name,
		Code:        r.Code,
		Description: r.Description,
		CreatedAt:   timestamppb.New(r.CreatedAt),
		UpdatedAt:   timestamppb.New(r.UpdatedAt),
	}, nil
}

// CreateClassPayload is a type
type CreateClassPayload struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

// Validate is a method
func (c CreateClassPayload) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.Name, validation.Required, validation.Length(3, 100)),
	)
}
