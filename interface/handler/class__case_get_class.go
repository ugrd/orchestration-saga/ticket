package handler

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/catalog/pkg/common"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// GetListClass is a method
func (hdl *Handler) GetListClass(ctx context.Context, req *catalog.ClassFilterQuery) (*catalog.ListClass, error) {
	var classes []*catalog.Class

	rows, err := hdl.Dependency.Repository.Class.FindAll(ctx, &common.FilterQuery{})
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	for _, row := range rows {
		classes = append(classes, &catalog.Class{
			Id:          uint64(row.ID),
			Name:        row.Name,
			Code:        row.Code,
			Description: row.Description,
			CreatedAt:   timestamppb.New(row.CreatedAt),
			UpdatedAt:   timestamppb.New(row.UpdatedAt),
		})
	}

	return &catalog.ListClass{
		Classes: classes,
	}, nil
}
