package core

import (
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
)

// GRPCService collects grpc service server
type GRPCService struct {
	catalog.UnimplementedClassServiceServer
	catalog.UnimplementedTransportServiceServer
	catalog.UnimplementedProviderServiceServer
	catalog.UnimplementedPassengerServiceServer
	catalog.UnimplementedTransportShelterServiceServer
	catalog.UnimplementedTicketServiceServer
}
