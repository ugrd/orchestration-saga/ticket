package handler

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/catalog/pkg/common"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// GetListPassenger is a method
func (hdl *Handler) GetListPassenger(ctx context.Context, _ *catalog.PassengerFilterQuery) (*catalog.ListPassenger, error) {
	var passengers []*catalog.Passenger

	rows, err := hdl.Dependency.Repository.PassengerRepo.FindAll(ctx, &common.FilterQuery{})
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	for _, row := range rows {
		passengers = append(passengers, &catalog.Passenger{
			Id:        uint64(row.ID),
			Name:      row.Name,
			AgeMax:    uint32(row.AgeMax),
			AgeMin:    uint32(row.AgeMin),
			CreatedAt: timestamppb.New(row.CreatedAt),
			UpdatedAt: timestamppb.New(row.UpdatedAt),
		})
	}

	return &catalog.ListPassenger{
		Passengers: passengers,
	}, nil
}
