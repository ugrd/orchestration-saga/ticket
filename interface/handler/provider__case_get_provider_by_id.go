package handler

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"
	"reflect"
)

// GetProviderByID is method
func (hdl *Handler) GetProviderByID(ctx context.Context, req *catalog.ProviderIdentifierRequest) (*catalog.Provider, error) {
	id := req.GetId()
	if reflect.ValueOf(id).IsZero() {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(map[string]interface{}{
				"id": "is not valid",
			}).ErrList()
	}

	r, err := hdl.Dependency.Repository.ProviderRepo.FindByID(ctx, uint(id))
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Data not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	return &catalog.Provider{
		Id:          uint64(r.ID),
		Name:        r.Name,
		Code:        r.Code,
		Description: r.Description,
		CreatedAt:   timestamppb.New(r.CreatedAt),
		UpdatedAt:   timestamppb.New(r.UpdatedAt),
	}, nil
}
