package handler

import (
	"context"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"
)

// UpdateProvider is method
func (hdl *Handler) UpdateProvider(ctx context.Context, req *catalog.UpdateProviderRequest) (*catalog.Provider, error) {
	payload := &UpdateProviderPayload{
		ID:          req.GetId(),
		TransportID: req.GetTransportId(),
		Name:        req.GetName(),
		Description: req.GetDescription(),
	}

	if err := payload.Validate(); err != nil {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(utils.ErrToMap(err)).
			ErrList()
	}

	_, err := hdl.Dependency.Repository.Transport.FindByID(ctx, uint(req.GetTransportId()))
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Transport not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	r, errFindByID := hdl.Dependency.Repository.ProviderRepo.FindByID(ctx, uint(req.GetId()))
	if errFindByID != nil {
		switch errFindByID {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Data not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(errFindByID.Error()).
				Err()
		}
	}

	code := entity.ConvertToCode(req.GetName())

	_, errFindBySlug := hdl.Dependency.Repository.ProviderRepo.FindByCode(ctx, code)
	if errFindBySlug == nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.AlreadyExists).
			WithFieldsFromMap(map[string]interface{}{
				"name": fmt.Sprintf("already exist"),
			}).
			ErrList()
	}

	if r.Code == code {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.AlreadyExists).
			WithFieldsFromMap(map[string]interface{}{
				"name": fmt.Sprintf("already exist"),
			}).
			ErrList()
	}

	err = hdl.Dependency.Repository.ProviderRepo.Update(ctx, &entity.Provider{
		TransportID: uint(req.GetTransportId()),
		Name:        payload.Name,
		Code:        code,
		Description: payload.Description,
	}, uint(req.GetId()))
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	r, _ = hdl.Dependency.Repository.ProviderRepo.FindByID(ctx, uint(req.GetId()))
	return &catalog.Provider{
		Id:          uint64(r.ID),
		TransportId: uint64(r.TransportID),
		Name:        r.Name,
		Code:        r.Code,
		Description: r.Description,
		CreatedAt:   timestamppb.New(r.CreatedAt),
		UpdatedAt:   timestamppb.New(r.UpdatedAt),
	}, nil
}

// UpdateProviderPayload is a type
type UpdateProviderPayload struct {
	ID          uint64 `json:"id"`
	TransportID uint64 `json:"transport_id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

// Validate is a method
func (c UpdateProviderPayload) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.ID, validation.Required),
		validation.Field(&c.TransportID, validation.Required),
		validation.Field(&c.Name, validation.Required, validation.Length(3, 100)),
	)
}
