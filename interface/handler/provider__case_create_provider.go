package handler

import (
	"context"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"
)

// CreateProvider is a method
func (hdl *Handler) CreateProvider(ctx context.Context, req *catalog.CreateProviderRequest) (*catalog.Provider, error) {
	payload := &CreateProviderPayload{
		TransportID: req.GetTransportId(),
		Name:        req.GetName(),
		Description: req.GetDescription(),
	}

	if err := payload.Validate(); err != nil {
		return nil, exception.NewGRPCError().
			WithFieldsFromMap(utils.ErrToMap(err)).
			ErrList()
	}

	_, err := hdl.Dependency.Repository.Transport.FindByID(ctx, uint(req.GetTransportId()))
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.NotFound).
				WithMsg(fmt.Sprintf("Data not found")).
				Err()
		default:
			return nil, exception.NewGRPCError().
				WithErrCode(codes.Unknown).
				WithMsg(err.Error()).
				Err()
		}
	}

	code := entity.ConvertToCode(req.GetName())
	_, ok := hdl.Dependency.Repository.ProviderRepo.FindByCode(ctx, code)
	if ok == nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.AlreadyExists).
			WithFieldsFromMap(map[string]interface{}{
				"name": fmt.Sprintf("already exist"),
			}).
			ErrList()
	}

	r := &entity.Provider{
		TransportID: uint(req.GetTransportId()),
		Name:        req.GetName(),
		Code:        code,
		Description: req.GetDescription(),
	}

	err = hdl.Dependency.Repository.ProviderRepo.Create(ctx, r)
	if err != nil {
		return nil, exception.NewGRPCError().
			WithErrCode(codes.Unknown).
			WithMsg(err.Error()).
			Err()
	}

	return &catalog.Provider{
		Id:          uint64(r.ID),
		Name:        r.Name,
		Code:        r.Code,
		Description: r.Description,
		CreatedAt:   timestamppb.New(r.CreatedAt),
		UpdatedAt:   timestamppb.New(r.UpdatedAt),
	}, nil
}

// CreateProviderPayload is a type
type CreateProviderPayload struct {
	TransportID uint64 `json:"transport_id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

// Validate is a method
func (c CreateProviderPayload) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.TransportID, validation.Required),
		validation.Field(&c.Name, validation.Required, validation.Length(3, 100)),
	)
}
