package validator

// ChainError is a type
type ChainError error

// ChainHandle is a function
func ChainHandle(chains ...ChainError) error {
	for _, c := range chains {
		if err := c; err != nil {
			return err
		}
	}

	return nil
}
