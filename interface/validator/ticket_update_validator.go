package validator

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"regexp"
)

// UpdateTicketPayload is a method
type UpdateTicketPayload struct {
	ID            uint64
	TransportID   uint64
	ClassID       uint64
	OriginID      uint64
	DestinationID uint64
	ProviderID    uint64
	DepartureTime string
	ArriveTime    string
	Price         uint64
	Quantity      uint32
}

// Validate is a method
func (c UpdateTicketPayload) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.ID, validation.Required),
		validation.Field(&c.TransportID, validation.Required),
		validation.Field(&c.ClassID, validation.Required),
		validation.Field(&c.OriginID, validation.Required),
		validation.Field(&c.OriginID, validation.Required),
		validation.Field(&c.ProviderID, validation.Required),
		validation.Field(&c.DepartureTime, validation.Required, validation.Match(regexp.MustCompile(`^\d{4}/\d{2}/\d{2} \d{2}:\d{2}$`))),
		validation.Field(&c.ArriveTime, validation.Required, validation.Match(regexp.MustCompile(`^\d{4}/\d{2}/\d{2} \d{2}:\d{2}$`))),
		validation.Field(&c.Price, validation.Required),
		validation.Field(&c.Quantity, validation.Required),
	)
}
