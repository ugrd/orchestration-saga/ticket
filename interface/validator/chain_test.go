package validator_test

import (
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/catalog/interface/validator"
	"testing"
	"unicode"
)

func TestChain(t *testing.T) {
	err := validator.ChainHandle(
		tChain1(10),
		tChain2(10),
	)

	fmt.Println(err)

	str := "nlqpqIQMro"
	runes := []rune(str)
	runes[0] = unicode.ToUpper(runes[0])

	fmt.Println(string(runes))
}

func tChain1(id int) error {
	fmt.Println(id)

	return nil
}

func tChain2(id int64) error {
	fmt.Println(id)

	return fmt.Errorf("Hello")
}
