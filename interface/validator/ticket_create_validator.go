package validator

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"regexp"
)

// CreateTicketPayload is a method
type CreateTicketPayload struct {
	TransportID   uint64 `json:"transport_id"`
	ClassID       uint64 `json:"class_id"`
	OriginID      uint64 `json:"origin_id"`
	DestinationID uint64 `json:"destination_id"`
	ProviderID    uint64 `json:"provider_id"`
	DepartureTime string `json:"departure_time"`
	ArriveTime    string `json:"arrive_time"`
	Price         uint64 `json:"price"`
	Quantity      uint32 `json:"quantity"`
}

// Validate is a method
func (c CreateTicketPayload) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.TransportID, validation.Required),
		validation.Field(&c.ClassID, validation.Required),
		validation.Field(&c.OriginID, validation.Required),
		validation.Field(&c.OriginID, validation.Required),
		validation.Field(&c.ProviderID, validation.Required),
		validation.Field(&c.DepartureTime, validation.Required, validation.Match(regexp.MustCompile(`^\d{4}/\d{2}/\d{2} \d{2}:\d{2}$`))),
		validation.Field(&c.ArriveTime, validation.Required, validation.Match(regexp.MustCompile(`^\d{4}/\d{2}/\d{2} \d{2}:\d{2}$`))),
		validation.Field(&c.Price, validation.Required),
		validation.Field(&c.Quantity, validation.Required),
	)
}
