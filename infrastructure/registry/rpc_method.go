package registry

import "gitlab.com/ugrd/orchestration-saga/package/grpc/auth"

// MethodOwner is a method
type MethodOwner struct {
	Owner     []string `json:"owner"`
	Protected bool     `json:"protected"`
}

// RPCMethods is a function to hold grpc service methods
// false value indicates that the method is not protected (no authorization needed)
func RPCMethods() auth.MethodTypes {
	return map[string]*auth.MethodOwner{
		// ticket service methods
		"/master.catalog.TicketService/CreateTicket":  {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.catalog.TicketService/UpdateTicket":  {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.catalog.TicketService/DeleteTicket":  {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.catalog.TicketService/GetTicketByID": {Owners: []string{"USER", "ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.catalog.TicketService/GetTicketByNo": {Owners: []string{"USER", "ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.catalog.TicketService/GetListTicket": {Owners: []string{"USER", "ADMIN", "SUPER_ADMIN"}, Protect: true},
		// class service methods
		"/master.account.ClassService/CreateClass":  {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.ClassService/UpdateClass":  {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.ClassService/DeleteClass":  {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.ClassService/GetClassByID": {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.ClassService/GetListClass": {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		// passenger service methods
		"/master.account.PassengerService/CreatePassenger":  {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.PassengerService/UpdatePassenger":  {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.PassengerService/DeletePassenger":  {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.PassengerService/GetPassengerByID": {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.PassengerService/GetListPassenger": {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		// transport service methods
		"/master.account.TransportService/CreateTransport":  {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.TransportService/UpdateTransport":  {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.TransportService/DeleteTransport":  {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.TransportService/GetTransportByID": {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.TransportService/GetListTransport": {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		// provider service methods
		"/master.account.ProviderService/CreateProvider":  {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.ProviderService/UpdateProvider":  {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.ProviderService/DeleteProvider":  {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.ProviderService/GetProviderByID": {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.ProviderService/GetListProvider": {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		// transport shelter service methods
		"/master.account.TransportShelterService/CreateTransportShelter":  {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.TransportShelterService/UpdateTransportShelter":  {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.TransportShelterService/DeleteTransportShelter":  {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.TransportShelterService/GetTransportShelterByID": {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
		"/master.account.TransportShelterService/GetListTransportShelter": {Owners: []string{"ADMIN", "SUPER_ADMIN"}, Protect: true},
	}
}
