package registry

import (
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/repository"
	"gitlab.com/ugrd/orchestration-saga/catalog/infrastructure/persistence"
	"gorm.io/gorm"
)

// Repositories is a struct which collect repositories
type Repositories struct {
	DB                   *gorm.DB
	Transport            repository.TransportRepoInterface
	Class                repository.ClassRepoInterface
	ProviderRepo         repository.ProviderRepoInterface
	PassengerRepo        repository.PassengerRepoInterface
	TransportShelterRepo repository.TransportShelterRepoInterface
	TicketRepo           repository.TicketRepoInterface
}

// NewRepo is constructor
func NewRepo(db *gorm.DB) *Repositories {
	return &Repositories{
		DB:                   db,
		Transport:            persistence.NewTransportRepo(db),
		Class:                persistence.NewClassRepo(db),
		ProviderRepo:         persistence.NewProviderRepo(db),
		PassengerRepo:        persistence.NewPassengerRepo(db),
		TransportShelterRepo: persistence.NewTransportShelterRepo(db),
		TicketRepo:           persistence.NewTicketRepo(db),
	}
}
