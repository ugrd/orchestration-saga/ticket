package registry

import (
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/contract"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
)

// CollectEntities is function collects entities
func CollectEntities() []contract.Entity {
	return []contract.Entity{
		{Entity: entity.Transport{}},
		{Entity: entity.Provider{}},
		{Entity: entity.Class{}},
		{Entity: entity.Passenger{}},
		{Entity: entity.TransportShelter{}},
		{Entity: entity.Ticket{}},
	}
}

// CollectTables is function collects entity names
func CollectTables() []contract.Table {
	var transport entity.Transport
	var class entity.Class
	var provider entity.Provider
	var passenger entity.Passenger
	var transportShelter entity.TransportShelter
	var ticket entity.Ticket

	return []contract.Table{
		{Name: transport.TableName()},
		{Name: class.TableName()},
		{Name: provider.TableName()},
		{Name: passenger.TableName()},
		{Name: transportShelter.TableName()},
		{Name: ticket.TableName()},
	}
}
