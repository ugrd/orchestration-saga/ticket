package persistence

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/repository"
	"gitlab.com/ugrd/orchestration-saga/catalog/pkg/common"
	"gorm.io/gorm"
)

// TransportShelterRepo is a struct
type TransportShelterRepo struct {
	db *gorm.DB
}

// Create is a method to create new role
func (r TransportShelterRepo) Create(ctx context.Context, transportShelter *entity.TransportShelter) error {
	return r.db.WithContext(ctx).Create(transportShelter).Error
}

// Update is a method to update existing role
func (r TransportShelterRepo) Update(ctx context.Context, transportShelter *entity.TransportShelter, id uint) error {
	return r.db.WithContext(ctx).Where("id = ?", id).Updates(transportShelter).Error
}

// Delete is a method to remove existing role
func (r TransportShelterRepo) Delete(ctx context.Context, id uint) error {
	return r.db.WithContext(ctx).Delete(&entity.TransportShelter{}, id).Error
}

// FindByID is a method to retrieve transport by id
func (r TransportShelterRepo) FindByID(ctx context.Context, id uint) (*entity.TransportShelter, error) {
	var transportShelter entity.TransportShelter

	err := r.db.WithContext(ctx).
		Joins("Transport").
		Where("transport_shelters.id = ?", id).
		Take(&transportShelter).Error
	if err != nil {
		return nil, err
	}

	return &transportShelter, nil
}

// FindByName is a method to retrieve transport by slug
func (r TransportShelterRepo) FindByName(ctx context.Context, name string) (*entity.TransportShelter, error) {
	var transportShelter entity.TransportShelter

	err := r.db.WithContext(ctx).
		Joins("Transport").
		Where("transport_shelters.name = ?", name).
		Take(&transportShelter).Error
	if err != nil {
		return nil, err
	}

	return &transportShelter, nil
}

// FindByCode is a method
func (r TransportShelterRepo) FindByCode(ctx context.Context, code string) (*entity.TransportShelter, error) {
	var transportShelter entity.TransportShelter

	err := r.db.WithContext(ctx).
		Joins("Transport").
		Where("transport_shelters.code = ?", code).
		Take(&transportShelter).Error
	if err != nil {
		return nil, err
	}

	return &transportShelter, nil
}

// FindByTransportID is a method to retrieve transport by slug
func (r TransportShelterRepo) FindByTransportID(ctx context.Context, transportID uint) ([]*entity.TransportShelter, error) {
	var transportShelters []*entity.TransportShelter

	err := r.db.WithContext(ctx).
		Joins("Transport").
		Where("transport_shelters.transport_id = ?", transportID).
		Find(&transportShelters).Error
	if err != nil {
		return nil, err
	}

	return transportShelters, nil
}

// FindAll is a method to retrieve transports
func (r TransportShelterRepo) FindAll(ctx context.Context, _ *common.FilterQuery) ([]*entity.TransportShelter, error) {
	var transportShelters []*entity.TransportShelter

	err := r.db.WithContext(ctx).Find(&transportShelters).Error
	if err != nil {
		return nil, err
	}

	return transportShelters, nil
}

// NewTransportShelterRepo is a constructor
func NewTransportShelterRepo(db *gorm.DB) *TransportShelterRepo {
	return &TransportShelterRepo{db: db}
}

var _ repository.TransportShelterRepoInterface = &TransportShelterRepo{}
