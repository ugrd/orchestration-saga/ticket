package persistence

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/repository"
	"gitlab.com/ugrd/orchestration-saga/catalog/pkg/common"
	"gorm.io/gorm"
)

// TransportRepo is a struct
type TransportRepo struct {
	db *gorm.DB
}

// Create is a method to create new role
func (r TransportRepo) Create(ctx context.Context, transport *entity.Transport) error {
	return r.db.WithContext(ctx).Create(transport).Error
}

// Update is a method to update existing role
func (r TransportRepo) Update(ctx context.Context, transport *entity.Transport, id uint) error {
	return r.db.WithContext(ctx).Where("id = ?", id).Updates(transport).Error
}

// Delete is a method to remove existing role
func (r TransportRepo) Delete(ctx context.Context, id uint) error {
	return r.db.WithContext(ctx).Delete(&entity.Transport{}, id).Error
}

// FindByID is a method to retrieve transport by id
func (r TransportRepo) FindByID(ctx context.Context, id uint) (*entity.Transport, error) {
	var transport entity.Transport

	err := r.db.WithContext(ctx).
		Where("id = ?", id).
		Take(&transport).Error
	if err != nil {
		return nil, err
	}

	return &transport, nil
}

// FindBySlug is a method to retrieve transport by slug
func (r TransportRepo) FindBySlug(ctx context.Context, slug string) (*entity.Transport, error) {
	var transport entity.Transport

	err := r.db.WithContext(ctx).
		Where("slug = ?", slug).
		Take(&transport).Error
	if err != nil {
		return nil, err
	}

	return &transport, nil
}

// FindAll is a method to retrieve transports
func (r TransportRepo) FindAll(ctx context.Context, _ *common.FilterQuery) ([]*entity.Transport, error) {
	var transports []*entity.Transport

	err := r.db.WithContext(ctx).Find(&transports).Error
	if err != nil {
		return nil, err
	}

	return transports, nil
}

// NewTransportRepo is a constructor
func NewTransportRepo(db *gorm.DB) *TransportRepo {
	return &TransportRepo{db: db}
}

var _ repository.TransportRepoInterface = &TransportRepo{}
