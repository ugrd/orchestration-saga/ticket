package persistence

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/repository"
	"gitlab.com/ugrd/orchestration-saga/catalog/pkg/common"
	"gorm.io/gorm"
)

// TicketRepo is a struct
type TicketRepo struct {
	db *gorm.DB
}

// Create is a method
func (t *TicketRepo) Create(ctx context.Context, ticket *entity.Ticket) error {
	return t.db.WithContext(ctx).Create(ticket).Error
}

// Update is a method
func (t *TicketRepo) Update(ctx context.Context, ticket *entity.Ticket, id uint) error {
	return t.db.WithContext(ctx).Where("id = ?", id).Updates(ticket).Error
}

// Delete is a method
func (t *TicketRepo) Delete(ctx context.Context, id uint) error {
	return t.db.WithContext(ctx).Delete(&entity.Ticket{}, id).Error
}

// FindByID is a method
func (t *TicketRepo) FindByID(ctx context.Context, id uint) (*entity.Ticket, error) {
	var ticket entity.Ticket

	err := t.db.WithContext(ctx).
		Joins("Transport").
		Joins("Class").
		Joins("Origin").
		Joins("Destination").
		Joins("Provider").
		Where("tickets.id = ?", id).
		Take(&ticket).Error
	if err != nil {
		return nil, err
	}

	return &ticket, nil
}

// FindByNo is a method
func (t *TicketRepo) FindByNo(ctx context.Context, no string) (*entity.Ticket, error) {
	var ticket entity.Ticket

	err := t.db.WithContext(ctx).
		Joins("Transport").
		Joins("Class").
		Joins("Origin").
		Joins("Destination").
		Joins("Provider").
		Where("tickets.no = ?", no).
		Take(&ticket).Error
	if err != nil {
		return nil, err
	}

	return &ticket, nil
}

// FindAll is a method
func (t *TicketRepo) FindAll(ctx context.Context, filter *common.FilterQuery) ([]*entity.Ticket, error) {
	var tickets []*entity.Ticket

	err := t.db.WithContext(ctx).
		Joins("Class").
		Joins("Transport").
		Joins("Origin").
		Joins("Destination").
		Joins("Provider").
		//Where("tickets.no LIKE ?", "%s"+filter.Query+"%s").
		Offset(int(filter.Page * filter.Limit)).
		//Limit(int(filter.Limit)).
		Find(&tickets).Error
	if err != nil {
		return nil, err
	}

	return tickets, nil
}

// FindSearch is a method
func (t *TicketRepo) FindSearch(ctx context.Context, query *entity.SearchFilterQuery) ([]*entity.Ticket, error) {
	//TODO implement me
	panic("implement me")
}

// NewTicketRepo is a constructor
func NewTicketRepo(db *gorm.DB) *TicketRepo {
	return &TicketRepo{db: db}
}

var _ repository.TicketRepoInterface = &TicketRepo{}
