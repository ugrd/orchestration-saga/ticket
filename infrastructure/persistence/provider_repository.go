package persistence

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/repository"
	"gitlab.com/ugrd/orchestration-saga/catalog/pkg/common"
	"gorm.io/gorm"
)

// ProviderRepo is a struct
type ProviderRepo struct {
	db *gorm.DB
}

// Create is a method to create new role
func (r ProviderRepo) Create(ctx context.Context, provider *entity.Provider) error {
	return r.db.WithContext(ctx).Create(provider).Error
}

// Update is a method to update existing role
func (r ProviderRepo) Update(ctx context.Context, provider *entity.Provider, id uint) error {
	return r.db.WithContext(ctx).Where("id = ?", id).Updates(provider).Error
}

// Delete is a method to remove existing role
func (r ProviderRepo) Delete(ctx context.Context, id uint) error {
	return r.db.WithContext(ctx).Delete(&entity.Provider{}, id).Error
}

// FindByID is a method to retrieve category by id
func (r ProviderRepo) FindByID(ctx context.Context, id uint) (*entity.Provider, error) {
	var provider entity.Provider

	err := r.db.WithContext(ctx).
		Joins("Transport").
		Where("providers.id = ?", id).
		Take(&provider).Error
	if err != nil {
		return nil, err
	}

	return &provider, nil
}

// FindByCode is a method to retrieve category by code
func (r ProviderRepo) FindByCode(ctx context.Context, code string) (*entity.Provider, error) {
	var provider entity.Provider

	err := r.db.WithContext(ctx).
		Joins("Transport").
		Where("providers.code = ?", code).
		Take(&provider).Error
	if err != nil {
		return nil, err
	}

	return &provider, nil
}

// FindAll is a method to retrieve categories
func (r ProviderRepo) FindAll(ctx context.Context, _ *common.FilterQuery) ([]*entity.Provider, error) {
	var providers []*entity.Provider

	err := r.db.WithContext(ctx).
		Joins("Transport").
		Find(&providers).Error
	if err != nil {
		return nil, err
	}

	return providers, nil
}

// NewProviderRepo is a constructor
func NewProviderRepo(db *gorm.DB) *ProviderRepo {
	return &ProviderRepo{db: db}
}

var _ repository.ProviderRepoInterface = &ProviderRepo{}
