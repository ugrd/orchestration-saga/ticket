package persistence

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/repository"
	"gitlab.com/ugrd/orchestration-saga/catalog/pkg/common"
	"gorm.io/gorm"
)

// ClassRepo is a struct
type ClassRepo struct {
	db *gorm.DB
}

// Create is a method to create new role
func (r ClassRepo) Create(ctx context.Context, service *entity.Class) error {
	return r.db.WithContext(ctx).Create(service).Error
}

// Update is a method to update existing role
func (r ClassRepo) Update(ctx context.Context, service *entity.Class, id uint) error {
	return r.db.WithContext(ctx).Where("id = ?", id).Updates(service).Error
}

// Delete is a method to remove existing role
func (r ClassRepo) Delete(ctx context.Context, id uint) error {
	return r.db.WithContext(ctx).Delete(&entity.Class{}, id).Error
}

// FindByID is a method to retrieve category by id
func (r ClassRepo) FindByID(ctx context.Context, id uint) (*entity.Class, error) {
	var service entity.Class

	err := r.db.WithContext(ctx).
		Where("id = ?", id).
		Take(&service).Error
	if err != nil {
		return nil, err
	}

	return &service, nil
}

// FindByCode is a method to retrieve category by code
func (r ClassRepo) FindByCode(ctx context.Context, code string) (*entity.Class, error) {
	var service entity.Class

	err := r.db.WithContext(ctx).
		Where("code = ?", code).
		Take(&service).Error
	if err != nil {
		return nil, err
	}

	return &service, nil
}

// FindAll is a method to retrieve categories
func (r ClassRepo) FindAll(ctx context.Context, _ *common.FilterQuery) ([]*entity.Class, error) {
	var services []*entity.Class

	err := r.db.WithContext(ctx).Find(&services).Error
	if err != nil {
		return nil, err
	}

	return services, nil
}

// NewClassRepo is a constructor
func NewClassRepo(db *gorm.DB) *ClassRepo {
	return &ClassRepo{db: db}
}

var _ repository.ClassRepoInterface = &ClassRepo{}
