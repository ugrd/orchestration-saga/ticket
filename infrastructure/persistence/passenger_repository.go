package persistence

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/repository"
	"gitlab.com/ugrd/orchestration-saga/catalog/pkg/common"
	"gorm.io/gorm"
	"strings"
)

// PassengerRepo is a struct
type PassengerRepo struct {
	db *gorm.DB
}

// Create is a method to create record
func (r PassengerRepo) Create(ctx context.Context, passenger *entity.Passenger) error {
	return r.db.WithContext(ctx).Create(passenger).Error
}

// Update is a method to update existing record
func (r PassengerRepo) Update(ctx context.Context, passenger *entity.Passenger, id uint) error {
	return r.db.WithContext(ctx).Where("id = ?", id).Updates(passenger).Error
}

// Delete is a method to remove existing record by id
func (r PassengerRepo) Delete(ctx context.Context, id uint) error {
	return r.db.WithContext(ctx).Delete(&entity.Class{}, id).Error
}

// FindByID is a method to retrieve record by id
func (r PassengerRepo) FindByID(ctx context.Context, id uint) (*entity.Passenger, error) {
	var passenger entity.Passenger

	err := r.db.WithContext(ctx).
		Where("id = ?", id).
		Take(&passenger).Error
	if err != nil {
		return nil, err
	}

	return &passenger, nil
}

// FindByName is a method to retrieve record by name
func (r PassengerRepo) FindByName(ctx context.Context, name string) (*entity.Passenger, error) {
	var passenger entity.Passenger

	err := r.db.WithContext(ctx).
		Where("lower(name) = ?", strings.ToLower(name)).
		Take(&passenger).Error
	if err != nil {
		return nil, err
	}

	return &passenger, nil
}

// FindAll is a method to retrieve categories
func (r PassengerRepo) FindAll(ctx context.Context, _ *common.FilterQuery) ([]*entity.Passenger, error) {
	var passengers []*entity.Passenger

	err := r.db.WithContext(ctx).Find(&passengers).Error
	if err != nil {
		return nil, err
	}

	return passengers, nil
}

// NewPassengerRepo is a constructor
func NewPassengerRepo(db *gorm.DB) *PassengerRepo {
	return &PassengerRepo{db: db}
}

var _ repository.PassengerRepoInterface = &PassengerRepo{}
