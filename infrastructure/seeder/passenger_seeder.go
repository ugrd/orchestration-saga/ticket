package seeder

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/constant"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/catalog/infrastructure/registry"
)

// PassengerSeeder is a type
type PassengerSeeder struct {
}

// Seed is a method
func (*PassengerSeeder) Seed(repo *registry.Repositories) error {
	passengers := constant.PassengerObjectList()
	for _, passenger := range passengers {
		r, _ := repo.PassengerRepo.FindByName(context.Background(), passenger.Name)
		if r != nil {
			continue
		}

		err := repo.PassengerRepo.Create(context.Background(), &entity.Passenger{
			Name:   passenger.Name,
			AgeMax: passenger.AgeMax,
			AgeMin: passenger.AgeMin,
		})

		if err != nil {
			return err
		}
	}
	return nil
}
