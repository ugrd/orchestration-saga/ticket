package seeder

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/constant"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/catalog/infrastructure/registry"
)

// ClassSeeder is a type
type ClassSeeder struct {
}

// Seed is a method
func (*ClassSeeder) Seed(repo *registry.Repositories) error {
	classes := constant.TransportClassList()
	for _, class := range classes {
		r, _ := repo.Class.FindByCode(context.Background(), class)
		if r != nil {
			continue
		}

		err := repo.Class.Create(context.Background(), &entity.Class{
			Name: class,
			Code: class,
		})

		if err != nil {
			return err
		}
	}

	return nil
}
