package seeder

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/catalog/infrastructure/registry"
)

// ProviderSeeder is a type
type ProviderSeeder struct {
}

// provider is a struct
type provider struct {
	Name        string `json:"name"`
	Code        string `json:"code"`
	Transport   string `json:"transport"`
	Description string `json:"description"`
}

var (
	defaultProviders = []provider{
		{
			Name:        "KA Sancaka",
			Code:        "ka-sancaka",
			Transport:   "train",
			Description: "-",
		},
		{
			Name:        "KA Ranggajati",
			Code:        "ka-ranggajati",
			Transport:   "train",
			Description: "-",
		},
		{
			Name:        "Air Asia",
			Code:        "air-asia",
			Transport:   "flight",
			Description: "-",
		},
		{
			Name:        "Sugeng Rahayu",
			Code:        "sugeng-rahayu",
			Transport:   "bus",
			Description: "-",
		},
	}
)

// Seed is a method
func (p *ProviderSeeder) Seed(repo *registry.Repositories) error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	for _, row := range defaultProviders {
		r, _ := repo.ProviderRepo.FindByCode(ctx, row.Code)
		if r != nil {
			continue
		}

		pr, _ := repo.Transport.FindBySlug(ctx, row.Transport)
		if pr == nil {
			return fmt.Errorf("provider not found")
		}

		err := repo.ProviderRepo.Create(ctx, &entity.Provider{
			TransportID: pr.ID,
			Name:        row.Name,
			Code:        row.Code,
			Description: row.Description,
		})

		if err != nil {
			return err
		}
	}

	return nil
}

var _ Seed = &ProviderSeeder{}
