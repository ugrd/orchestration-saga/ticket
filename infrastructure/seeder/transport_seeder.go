package seeder

import (
	"context"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/constant"
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/catalog/infrastructure/registry"
	"gitlab.com/ugrd/orchestration-saga/package/utils"
)

// TransportSeeder is a type
type TransportSeeder struct {
}

// Seed is a method
func (*TransportSeeder) Seed(repo *registry.Repositories) error {
	transports := constant.TransportList()
	for _, transport := range transports {
		slug := utils.StringToSlug(transport)

		r, _ := repo.Transport.FindBySlug(context.Background(), slug)
		if r != nil {
			continue
		}

		err := repo.Transport.Create(context.Background(), &entity.Transport{
			Name: transport,
			Slug: slug,
		})

		if err != nil {
			return err
		}
	}

	return nil
}
