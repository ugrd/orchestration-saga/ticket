package dto

import (
	"gitlab.com/ugrd/orchestration-saga/catalog/domain/entity"
	"gitlab.com/ugrd/orchestration-saga/proto/protobuf/master/catalog"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// TransformTicketToRPCResponse is a function
func TransformTicketToRPCResponse(payload *entity.Ticket) *catalog.Ticket {
	return &catalog.Ticket{
		Id:       uint64(payload.ID),
		No:       payload.No,
		Price:    payload.Price,
		Quantity: uint32(payload.Quantity),
		Transport: &catalog.TicketTransport{
			Id:          uint64(payload.Transport.ID),
			Name:        payload.Transport.Name,
			Slug:        payload.Transport.Slug,
			Description: payload.Transport.Description,
			CreatedAt:   timestamppb.New(payload.Transport.CreatedAt),
			UpdatedAt:   timestamppb.New(payload.Transport.UpdatedAt),
		},
		Class: &catalog.TicketClass{
			Id:          uint64(payload.Class.ID),
			Name:        payload.Class.Name,
			Code:        payload.Class.Code,
			Description: payload.Class.Description,
			CreatedAt:   timestamppb.New(payload.Class.CreatedAt),
			UpdatedAt:   timestamppb.New(payload.Class.UpdatedAt),
		},
		Provider: &catalog.TicketProvider{
			Id:          uint64(payload.Provider.ID),
			Name:        payload.Provider.Name,
			Code:        payload.Provider.Code,
			Description: payload.Provider.Description,
			CreatedAt:   timestamppb.New(payload.Provider.CreatedAt),
			UpdatedAt:   timestamppb.New(payload.Provider.UpdatedAt),
		},
		Origin: &catalog.TicketTransportShelter{
			Id:          uint64(payload.Origin.ID),
			TransportId: uint64(payload.Origin.TransportID),
			Name:        payload.Origin.Name,
			Code:        payload.Origin.Code,
			Province:    payload.Origin.Province,
			City:        payload.Origin.City,
			Address:     payload.Origin.Address,
			CreatedAt:   timestamppb.New(payload.Origin.CreatedAt),
			UpdatedAt:   timestamppb.New(payload.Origin.UpdatedAt),
		},
		Destination: &catalog.TicketTransportShelter{
			Id:          uint64(payload.Destination.ID),
			TransportId: uint64(payload.Destination.TransportID),
			Name:        payload.Destination.Name,
			Code:        payload.Destination.Code,
			Province:    payload.Destination.Province,
			City:        payload.Destination.City,
			Address:     payload.Destination.Address,
			CreatedAt:   timestamppb.New(payload.Destination.CreatedAt),
			UpdatedAt:   timestamppb.New(payload.Destination.UpdatedAt),
		},
		CreatedAt: timestamppb.New(payload.CreatedAt),
		UpdatedAt: timestamppb.New(payload.UpdatedAt),
		DeletedAt: timestamppb.New(payload.DeletedAt.Time),
	}
}

// TransformTicketListToRPCResponse is a function
func TransformTicketListToRPCResponse(payloads []*entity.Ticket) *catalog.ListTicket {
	var tickets []*catalog.Ticket
	for _, ticket := range payloads {
		tickets = append(tickets, TransformTicketToRPCResponse(ticket))
	}

	return &catalog.ListTicket{
		Tickets: tickets,
	}
}
