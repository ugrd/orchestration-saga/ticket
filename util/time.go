package util

import (
	"time"
)

const (
	layout = "2006/01/02 15:04"
)

// StringToTime is a method
func StringToTime(str string) (*time.Time, error) {
	datetime, err := time.Parse(layout, str)
	if err != nil {
		return nil, err
	}

	return &datetime, nil
}
